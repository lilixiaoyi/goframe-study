package user

import (
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gvalid"
	"goframe-study/app/model"
	"goframe-study/app/service"
)

type RegisterRes struct {
	Code  int         `json:"code"`
	Error string      `json:"error"`
	Data  interface{} `json:"data"`
}
type RegisterReq struct {
	Name  string `p:"username"  v:"required|length:6,30#请输入账号|账号长度为:min到:max位"`
	Pass  string `p:"password1" v:"required|length:6,30#请输入密码|密码长度不够"`
	Pass2 string `p:"password2" v:"required|length:6,30|same:password1#请确认密码|两次密码不一致"`
}

func Login(r *ghttp.Request) {
	id := r.GetInt64("id")
	result := service.UserLogin(id)
	//log.Fatal(r.Body)
	//r.Response.Writef("name: %v, pass: %v 结果 %v", r.Get("name"), r.Get("pass"), result)
	//r.Response.Writeln("处理用户登录!")
	r.Response.WriteJson(model.JsonResponse{
		Code:    200,
		Message: "查询用户信息",
		Data:    result,
	})
}

func Register(r *ghttp.Request) {
	//log.Fatal(r.Body)
	//参数解析 https://goframe.org/start/service
	var req *RegisterReq
	if err := r.Parse(&req); err != nil {
		// Validation error.
		if v, ok := err.(*gvalid.Error); ok {
			r.Response.WriteJsonExit(RegisterRes{
				Code:  1,
				Error: v.FirstString(),
			})
		}
		// Other error.
		r.Response.WriteJsonExit(RegisterRes{
			Code:  1,
			Error: err.Error(),
		})
	}
	// ...
	r.Response.WriteJsonExit(RegisterRes{
		Data: req,
	})
	//r.Response.Writeln("处理用户注册!")
}

func Get(r *ghttp.Request) {
	//log.Fatal(r.Body)
	r.Response.Writeln("获取单个用户信息!")
}
