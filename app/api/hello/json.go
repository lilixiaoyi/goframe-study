package hello

import (
	"github.com/gogf/gf/net/ghttp"
	"goframe-study/app/model"
)

func Json(r *ghttp.Request) {

	// format := &JsonResponse{Message: "消息不正确", Code: 500, Data: "任意格式数据都可以"}
	// r.Response.Writeln(format)

	r.Response.WriteJson(model.JsonResponse{
		Code:    200,
		Message: "message",
		Data:    "responseData",
	})

}
func Data(r *ghttp.Request) {
	r.Response.Writeln("Data数据接口!")
}
