package service

import (
	"database/sql"
	"github.com/gogf/gf/frame/g"
	"goframe-study/app/model"
)

func UserLogin(id int64) *model.UserModel {
	db := g.DB()
	logger := g.Log()
	var user *model.UserModel
	// 其实是有对dao封装的操作 https://gitee.com/-/ide/project/goflyfox/gfstudy/edit/master/-/17.gfcli/app/model/user/user_model.go
	err := db.Table("users").Where("id", id).Scan(&user)
	if err != nil && err != sql.ErrNoRows {
		logger.Header(false).Fatal(err)
	}
	if user != nil {
		logger.Header(false).Println(user)
	}
	return user
}
