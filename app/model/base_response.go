package model

//JsonResponse 基于的返回类型
type JsonResponse struct {
	Message string      `json:"message"`
	Code    int         `json:"code"` // 错误码((0:成功, 1:失败, >1:错误码))
	Data    interface{} `json:"data"`
}
