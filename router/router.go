package router

import (
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"goframe-study/app/api/hello"
	"goframe-study/app/api/user"
)

// 初始化路由信息
func init() {
	s := g.Server()
	s.Group("/", func(group *ghttp.RouterGroup) {
		group.ALL("/", hello.Hello)
		group.GET("/json", hello.Json)
		group.GET("/data", hello.Data)
	})
	s.Group("/user", func(group *ghttp.RouterGroup) {
		group.ALL("/login", user.Login)
		group.GET("/register", user.Register)
		group.GET("/get", user.Get)
	})
}
