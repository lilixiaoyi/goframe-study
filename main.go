package main

import (
	"github.com/gogf/gf/frame/g"
	_ "goframe-study/boot"
	_ "goframe-study/router"
)

func main() {
	g.Server().Run()
}
